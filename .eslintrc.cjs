// .eslintrc.js
module.exports = {
  root: true,
  env: {
    browser: true,
    es2021: true,
    node: true,
  },
  extends: [
    'plugin:@typescript-eslint/recommended',
    'plugin:nuxt/recommended',
    'plugin:vue/vue3-recommended',
    'plugin:prettier/recommended',
    'plugin:tailwindcss/recommended',
  ],
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: '@typescript-eslint/parser',
    ecmaVersion: 'latest',
    sourceType: 'module',
  },
  plugins: [
    '@typescript-eslint',
    'tailwindcss',
    'eslint-plugin-prettier',
    'eslint-plugin-nuxt',
  ],
  rules: {
    semi: 'error',
    quotes: ['error', 'single'],
    indent: ['error', 2],
    'comma-dangle': ['error', 'only-multiline'],
    'vue/html-self-closing': 'off',
    'vue/multi-word-component-names': 'off',
    'space-before-function-paren': 'off',
    'arrow-parens': 'off',
    'vue/attribute-hyphenation': 'off',
    'vue/no-setup-props-destructure': 'off',
    'vue/attributes-order': [
      'error',
      {
        order: [
          'DEFINITION',
          'LIST_RENDERING',
          'CONDITIONALS',
          'RENDER_MODIFIERS',
          'GLOBAL',
          ['UNIQUE', 'SLOT'],
          'TWO_WAY_BINDING',
          'OTHER_DIRECTIVES',
          'OTHER_ATTR',
          'EVENTS',
          'CONTENT',
        ],
        alphabetical: false,
      },
    ],
  },
};
