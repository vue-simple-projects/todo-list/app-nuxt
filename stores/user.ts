import { defineStore } from 'pinia';
import { type IUser, useUserApi } from '~/entities/user';

export const useUserStore = defineStore('user', () => {
  const user = ref<IUser>();
  const activated = computed(() => user.value?.activated || false);
  const { getCurrent } = useUserApi();

  const fetchUserData = async () => {
    user.value = await getCurrent();
  };

  const resetUserData = () => {
    user.value = undefined;
  };

  return {
    user,
    activated,
    fetchUserData,
    resetUserData,
  };
});
