import { defineStore } from 'pinia';
import { type Popup, type PopupName } from '~/entities/popups';

export const usePopupsStore = defineStore('popups', () => {
  const popups = reactive<Popup[]>([]);

  const showPopup = (id: PopupName, data: unknown = undefined) => {
    popups.push({ id, data });
  };

  const hidePopup = (id: PopupName) => {
    const idx = popups.findIndex((p) => p.id === id);
    if (~idx) {
      popups.splice(idx, 1);
    }
  };

  const getPopupData = (id: PopupName) => {
    return popups.find((p) => p.id === id)?.data ?? undefined;
  };

  return {
    popups,
    showPopup,
    hidePopup,
    getPopupData,
  };
});
