import { defineStore } from 'pinia';
import { appConfig, appEndpoints } from '~/shared/libs';
import {
  type AuthRequest,
  type AuthResponseError,
  useAuthApi,
} from '~entities/auth';
import { useUserStore } from '~/stores/user';
import { ValidateError } from 'async-validator';

type AuthErrors = {
  has: boolean;
  email: ValidateError[];
  password: ValidateError[];
  uknown: string;
};

export const useAuthStore = defineStore('auth', () => {
  const errors = reactive<AuthErrors>({
    has: false,
    email: [],
    password: [],
    uknown: '',
  });
  const { fetchUserData, resetUserData } = useUserStore();
  const checkAuthorization = () =>
    !!localStorage.getItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);

  const login = async (data: AuthRequest) => {
    tryResetErrors();
    const { login } = useAuthApi();
    try {
      await login(data);
      fetchUserData();
      navigateTo(appEndpoints.TASKS);
    } catch (error) {
      fillErrors(error as AuthResponseError);
    }
  };

  const tryResetErrors = () => {
    if (errors.has) {
      errors.has = false;
      errors.email = [];
      errors.password = [];
      errors.uknown = '';
    }
  };
  const fillErrors = (e: AuthResponseError) => {
    errors.has = true;
    if ((e && !!e?.errors) ?? false) {
      e?.errors?.forEach((e) => {
        const validateError: ValidateError = {
          message: e.msg,
          fieldValue: e.value,
          field: e.path,
        };
        if (e.path === 'email') {
          errors.email.push(validateError);
        } else if (e.path === 'password') {
          errors.password.push(validateError);
        } else {
          errors.uknown += validateError.message;
        }
      });
    } else {
      errors.uknown = e.message;
    }
  };

  const register = async (data: AuthRequest) => {
    tryResetErrors();
    const { register } = useAuthApi();
    try {
      await register(data);
      fetchUserData();
      navigateTo(appEndpoints.TASKS);
    } catch (error) {
      console.log('catch', error);
      errors.has = true;
      fillErrors(error as AuthResponseError);
    }
  };

  const logout = async () => {
    tryResetErrors();
    const { logout } = useAuthApi();
    try {
      await logout();
      resetUserData();
      navigateTo(appEndpoints.LOGIN);
    } catch (error) {
      console.log('catch', error);
      errors.has = true;
      fillErrors(error as AuthResponseError);
    }
  };
  return {
    errors,
    login,
    logout,
    register,
    checkAuthorization,
    tryResetErrors,
  };
});
