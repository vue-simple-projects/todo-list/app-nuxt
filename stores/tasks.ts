import { defineStore } from 'pinia';
import { type ITask, useTasksApi } from '~/entities/tasks';

export const useTasksStore = defineStore('tasks', () => {
  let fetched = false;
  const { fetch, update, updateMany, create, remove, removeMany } =
    useTasksApi();

  const tasks = ref<ITask[]>([]);
  const draggingTask = ref(false);
  const focusedTaskId = ref<string | null>(null);
  const lists = computed(() => getTasksByParent().reverse());
  const getTasksByParent = (parent: string | null = null) =>
    tasks.value.filter((t) => t.parent === parent) || [];
  const sortTasks = () => {
    tasks.value.sort((a, b) => (a.order ?? 0) - (b.order ?? 0));
  };
  const getTask = (id: string) => tasks.value.find((t) => t.id === id);

  const fetchTasks = async () => {
    if (fetched) {
      return;
    }
    fetched = true;

    try {
      tasks.value = (await fetch()) as ITask[];
      sortTasks();
    } catch (e) {
      console.error(e);
    }
  };

  const updateTask = async (task: ITask) => {
    try {
      await update(task);
      const updated = updateLocalTask(task);
      if (!updated) {
        fetched = false;
        fetch();
      }
    } catch (e) {
      console.error(e);
    }
  };

  const updateLocalTask = (task: ITask): boolean => {
    const idx = tasks.value.findIndex((t) => t.id === task.id);
    if (idx && tasks.value) {
      tasks.value[idx] = task;
      return true;
    }
    return false;
  };

  const addTask = async ({
    title = '',
    done = false,
    parent = null,
    order = undefined,
  }: ITask) => {
    const task = {
      title,
      done,
      parent,
      order: order === undefined ? getNextOrder(parent) : order,
    } as ITask;
    try {
      const newTask = await create(task);
      tasks.value.push(newTask as ITask);
      sortTasks();
      focusedTaskId.value = newTask.id;
    } catch (e) {
      console.error(e);
    }
  };

  const addEmptySiblingTask = (parent: string, siblingOrder: number) => {
    const orders = getTasksByParent(parent)
      .filter((t) => t.order > siblingOrder)
      .map((t) => t.order);
    const order = !!orders.length
      ? siblingOrder + Math.abs(siblingOrder - Math.min(...orders)) / 2
      : getNextOrder(parent);
    addTask({ order, parent } as ITask);
  };

  const getNextOrder = (parent: string | null) => {
    const children = getChildrenTasksByParentId(parent);
    return children.length;
  };

  const removeTask = async (id: string) => {
    try {
      await remove(id);
      tasks.value = tasks.value.filter((t) => t.id !== id);
    } catch (e) {
      console.error(e);
    }
  };

  const removeTaskWithChildren = async (id: string) => {
    try {
      const tasksForRemove = getChildrenTasksByParentId(id);
      const parent = getTask(id);
      if (parent) tasksForRemove.push(parent);
      removeMany(tasksForRemove);
      tasks.value = tasks.value.filter((t) => !tasksForRemove.includes(t));
    } catch (e) {
      console.error(e);
    }
  };

  const removeCompletedChildredTasks = async (id: string) => {
    try {
      const tasksForRemove = getChildrenTasksByParentId(id).filter(
        (t) => t.done,
      );
      if (!!tasksForRemove.length) {
        removeMany(tasksForRemove);
        tasks.value = tasks.value.filter((t) => !tasksForRemove.includes(t));
      }
    } catch (e) {
      console.error(e);
    }
  };

  const getChildrenTasksByParentId = (id: string): ITask[] => {
    const tasks = getTasksByParent(id);
    let children: ITask[] = [];
    children = children.concat(tasks);
    for (const task of tasks) {
      const t = getChildrenTasksByParentId(task.id);
      children = children.concat(t);
    }
    return children;
  };

  const moveTask = (
    id: string,
    parent: string | null | undefined,
    order: number | null = null,
  ) => {
    draggingTask.value = false;
    const task = getTask(id);
    if (task) {
      task.parent = parent;
      task.order = order ?? getNextOrder(parent);
    }
    const children = getTasksByParent(parent);
    normalizeTaskOrder(children);
    sortTasks();
    updateMany(children);
  };

  const normalizeTaskOrder = (tasks: ITask[]) => {
    tasks.sort((a, b) => (a.order ?? 0) - (b.order ?? 0));
    tasks.forEach((t, i) => {
      t.order = i;
    });
  };

  return {
    tasks,
    lists,
    focusedTaskId,
    draggingTask,
    getTasksByParent,
    fetchTasks,
    addTask,
    addEmptySiblingTask,
    updateTask,
    removeTask,
    removeCompletedChildredTasks,
    removeTaskWithChildren,
    moveTask,
  };
});
