type Options = {
  handle?: Ref<HTMLElement | ComponentPublicInstance | null>;
  data?: unknown;
  dropEffect?: 'move' | 'copy' | 'link' | 'none';
  effectAllowed?: 'move' | 'copy' | 'link' | 'none';
  onStart?: () => void;
  onEnd?: () => void;
  onMove?: () => void;
};

export const useDraggable = (
  target: Ref<HTMLElement | ComponentPublicInstance | null>,
  options: Options,
) => {
  const dragging = ref<boolean>(false);

  const start = () => {
    if (!target.value) return;

    const e = '$el' in target.value ? target.value.$el : target.value;
    e.setAttribute('draggable', 'true');
    options.onStart && options.onStart();
    dragging.value = true;
  };

  const end = () => {
    options.onEnd && options.onEnd();
    dragging.value = false;
    if (!target.value) return;
    const e = '$el' in target.value ? target.value.$el : target.value;
    e.removeAttribute('draggable');
  };

  watchEffect(() => {
    if (!target.value) return;
    const e = '$el' in target.value ? target.value.$el : target.value;
    e.ondragstart = (e: DragEvent) => {
      e.stopPropagation();
      if (e.dataTransfer) {
        e.dataTransfer.clearData();
        e.dataTransfer.setData(
          'text/plain',
          JSON.stringify({ id: 'useDraggable', data: options.data }),
        );
        e.dataTransfer.dropEffect = options?.dropEffect ?? 'move';
        e.dataTransfer.effectAllowed = options?.effectAllowed ?? 'move';
      }
    };
    e.ondrag = (e: DragEvent) => {
      e.stopPropagation();
      options.onMove && options?.onMove();
    };
    e.ondragend = (e: DragEvent) => {
      e.stopPropagation();
      end();
    };
  });
  const handle = options.handle ?? target;
  watchEffect(() => {
    if (!handle.value) return;
    const e = '$el' in handle.value ? handle.value.$el : handle.value;
    e.onmousedown = (e: DragEvent) => {
      e.stopPropagation();
      start();
    };
  });

  return {
    dragging,
  };
};
