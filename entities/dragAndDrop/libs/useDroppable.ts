type Options = {
  onDrop?: <T>(data: T) => void;
  onOver?: () => void;
  onEnter?: () => void;
  onLeave?: () => void;
};

export const useDroppable = <T>(
  target: Ref<HTMLElement | ComponentPublicInstance | null>,
  options: Options | null = null,
) => {
  const data = ref<T | null>();
  const overed = ref(false);
  watchEffect(() => {
    if (!target.value) return;
    const e = '$el' in target.value ? target.value.$el : target.value;
    e.ondrop = (e: DragEvent) => {
      e.preventDefault();
      e.stopPropagation();
      try {
        const d = JSON.parse(e.dataTransfer?.getData('text/plain') || '{}');
        if (!(d && 'id' in d && d.id === 'useDraggable')) return;
        data.value = d.data as T;
        options && options.onDrop && options?.onDrop(data.value);
      } catch (e) {
        console.log('error', e);
      }
    };
    e.ondragover = (e: Event) => {
      e.preventDefault();
      e.stopPropagation();
      options && options.onOver && options?.onOver();
    };
    e.ondragenter = (e: Event) => {
      e.stopPropagation();
      overed.value = true;
      options && options.onEnter && options?.onEnter();
    };
    e.ondragleave = (e: Event) => {
      e.stopPropagation();
      overed.value = false;
      options && options.onLeave && options?.onLeave();
    };
  });

  return {
    data,
    overed,
  };
};
