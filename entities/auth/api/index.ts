import { api, apiEndpoints } from '~shared/api';
import { type AuthRequest, type AuthResponse } from '../model';
import { appConfig } from '~/shared/libs';

const authApi = {
  login: async (data: AuthRequest): Promise<void> => {
    const response = await api.patch<AuthResponse>(apiEndpoints.AUTH, data);
    window.localStorage.setItem(
      appConfig.LOCAL_STORAGE_TOKEN_KEY,
      response.token,
    );
  },
  register: async (data: AuthRequest): Promise<void> => {
    const response = await api.post<AuthResponse>(apiEndpoints.AUTH, data);
    window.localStorage.setItem(
      appConfig.LOCAL_STORAGE_TOKEN_KEY,
      response.token,
    );
  },
  logout: async (): Promise<void> => {
    await api.delete<AuthResponse>(apiEndpoints.AUTH);
    window.localStorage.removeItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
  },
  refreshToken: async (): Promise<void> => {
    const response = await api.get<AuthResponse>(apiEndpoints.AUTH);
    window.localStorage.setItem(
      appConfig.LOCAL_STORAGE_TOKEN_KEY,
      response.token,
    );
  },
};

export const useAuthApi = () => authApi;
