type ResponseErrorDetails = {
  msg: string;
  path: string;
  value: string;
};
export type ResponseError = {
  message: string;
  errors: ResponseErrorDetails[];
};
