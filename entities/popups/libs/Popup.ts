import { PopupName } from './popupNames';

export type Popup = {
  id: PopupName;
  data?: unknown;
};
