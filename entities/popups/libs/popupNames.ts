export const USER_SETTINGS_POPUP = 'USER_SETTINGS_POPUP';
export const ERROR_POPUP = 'ERROR_POPUP';

const popupNames = [USER_SETTINGS_POPUP, ERROR_POPUP] as const;
export type PopupName = (typeof popupNames)[number];
