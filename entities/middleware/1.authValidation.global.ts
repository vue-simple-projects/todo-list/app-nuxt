import { appEndpoints } from '~shared/libs';
import { useAuthStore } from '~/stores/auth';

export default defineNuxtRouteMiddleware((to) => {
  const authEndpoints = [
    appEndpoints.LOGIN,
    appEndpoints.REGISTRATION,
    appEndpoints.RECOVERY_ACCOUNT,
    appEndpoints.RESET_PASSWORD,
  ] as string[];
  const includesAuthEndpoints = authEndpoints.includes(to.path);
  const authorized = useAuthStore().checkAuthorization();

  if (authorized && includesAuthEndpoints) {
    return navigateTo(appEndpoints.TASKS);
  } else if (!authorized && !includesAuthEndpoints) {
    return navigateTo(appEndpoints.LOGIN);
  }
});
