import { useTasksStore } from '~/stores/tasks';

export default defineNuxtRouteMiddleware(async () => {
  useTasksStore().fetchTasks();
});
