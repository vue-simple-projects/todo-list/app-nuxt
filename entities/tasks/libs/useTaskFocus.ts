import { useTasksStore } from '~/stores/tasks';
import { useFocus } from '@vueuse/core';

export const useTaskFocus = (id: Ref<string>, target: Ref) => {
  const { focused } = useFocus(target);
  const store = useTasksStore();

  const tryFocus = () => {
    if (id.value === store.focusedTaskId) {
      focused.value = true;
      store.focusedTaskId = null;
    }
  };

  watchEffect(tryFocus, { flush: 'post' });
};
