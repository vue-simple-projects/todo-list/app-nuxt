import { useTasksStore } from '~/stores/tasks';

export const useTryRemoveEmptyTask = (id: Ref<string>) => {
  const store = useTasksStore();
  const children = computed(() => store.getTasksByParent(id.value));

  const isValueEmptyAndNoChildren = (e: Event) => {
    const { value } = e.target as HTMLInputElement;
    return value === '' && !children.value.length;
  };

  const tryRemoveTaskIfValueEmpty = (e: Event) => {
    if (isValueEmptyAndNoChildren(e)) {
      e.preventDefault();
      store.removeTask(id.value);
    }
  };

  return { tryRemoveTaskIfValueEmpty };
};
