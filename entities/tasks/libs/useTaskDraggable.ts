import { useDraggable } from '~entities/dragAndDrop';
import { useTasksStore } from '~/stores/tasks';

export const useTaskDraggable = (id: Ref<string>) => {
  const store = useTasksStore();
  const dragHandle = ref<HTMLElement | null>(null);
  const dragTarget = ref<HTMLElement | null>(null);
  const dragging = ref(false);
  useDraggable(dragTarget, {
    handle: dragHandle,
    data: id.value,
    onStart: () => {
      store.draggingTask = true;
      setTimeout(() => {
        dragging.value = true;
      }, 250);
    },
    onEnd: () => {
      dragging.value = false;
      store.draggingTask = false;
    },
  });

  return { dragHandle, dragTarget, dragging };
};
