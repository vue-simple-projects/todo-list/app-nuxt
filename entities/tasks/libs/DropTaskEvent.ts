export type DropTaskEvent = {
  type: 'inside' | 'before' | 'after';
  taskId: string;
};
