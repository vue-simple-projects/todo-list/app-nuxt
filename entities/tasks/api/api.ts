import { api, apiEndpoints } from '~shared/api';
import { type ITaskDto } from '../model';

const tasksApi = {
  fetch: async (): Promise<ITaskDto[]> => {
    return api.get<ITaskDto[]>(apiEndpoints.TASKS);
  },
  create: async (task: ITaskDto): Promise<ITaskDto> => {
    return api.post<ITaskDto>(apiEndpoints.TASKS, task);
  },
  update: async (task: ITaskDto): Promise<void> => {
    api.put<ITaskDto>(apiEndpoints.task(task.id), task);
  },
  updateMany: async (tasks: ITaskDto[]): Promise<void> => {
    await api.put<ITaskDto>(apiEndpoints.TASKS, { tasks });
  },
  remove: async (id: string): Promise<void> => {
    await api.delete(apiEndpoints.task(id));
  },
  removeMany: async (tasks: ITaskDto[]): Promise<void> => {
    await api.delete(apiEndpoints.TASKS, { tasks });
  },
};

export const useTasksApi = () => tasksApi;
