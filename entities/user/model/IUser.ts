export interface IUser {
  id: string;
  email: string;
  readonly activated: boolean;
}
