import { api, apiEndpoints } from '~shared/api';
import { type IUser } from '../model';

const userApi = {
  getCurrent: async (): Promise<IUser> =>
    api.get<IUser>(apiEndpoints.CURRENT_USER),
  activate: async (code: string): Promise<IUser> =>
    api.post<IUser>(apiEndpoints.USER_ACTIVATION, code),
  getById: async (id: string): Promise<IUser> =>
    api.get<IUser>(apiEndpoints.user(id)),
};

export const useUserApi = () => userApi;
