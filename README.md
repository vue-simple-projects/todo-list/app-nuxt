# App Nuxt todo list

## Key technologies

- Nuxt 3
- tailwind
- typescript

## Functionality

- Registration, authorization and authentication (in progress)
- Create, edit, delete tasks (in progress)
- Drag and drop tasks (in progress)

## Setup and Run

- npm install
- npm run dev

### .env example

```env
VITE_API_URL="http://localhost:5700/api"
VITE_LOCAL_STORAGE_TOKEN_NAME="todo-list.access.token"
```

### Dockerfile example

```dockerfile
FROM node:alpine

WORKDIR /app
COPY package.json .
RUN npm install
COPY . .
CMD npm run dev
```
