// https://nuxt.com/docs/api/configuration/nuxt-config
import { fileURLToPath } from 'url';
import eslintPlugin from 'vite-plugin-eslint';
import { defineNuxtConfig } from 'nuxt/config';

export default defineNuxtConfig({
  vite: {
    plugins: [eslintPlugin()],
  },
  devtools: {
    enabled: true,
    timeline: {
      enabled: true,
    },
  },
  ssr: false,
  app: {
    layoutTransition: { name: 'layout', mode: 'out-in' },
    head: {
      meta: [
        { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      ],
      link: [
        {
          rel: 'stylesheet',
          href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap',
        },
      ],
      noscript: [
        // <noscript>JavaScript is required</noscript>
        { children: 'JavaScript is required' },
      ],
    },
  },
  modules: ['@nuxtjs/tailwindcss', '@pinia/nuxt', '@vueuse/nuxt'],
  css: ['~/assets/css/index.css'],
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  alias: {
    '~widgets': fileURLToPath(new URL('./widgets', import.meta.url)),
    '~features': fileURLToPath(new URL('./features', import.meta.url)),
    '~entities': fileURLToPath(new URL('./entities', import.meta.url)),
    '~shared': fileURLToPath(new URL('./shared', import.meta.url)),
    '~stores': fileURLToPath(new URL('./stores', import.meta.url)),
    '~assets': fileURLToPath(new URL('./assets', import.meta.url)),
    '~': fileURLToPath(new URL('./', import.meta.url)),
  },
  dir: {
    layouts: 'widgets/layouts',
    middleware: 'entities/middleware',
    public: 'shared/public',
  },
  typescript: {
    tsConfig: {
      compilerOptions: {
        types: ['@nuxt/types', '@pinia/nuxt'],
      },
    },
  },
});
