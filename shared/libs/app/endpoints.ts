export const appEndpoints = Object.freeze({
  ROOT: '/',
  LOGIN: '/auth/login',
  REGISTRATION: '/auth/registration',
  RECOVERY_ACCOUNT: '/auth/recovery',
  VERIFY_ACCOUNT: '/auth/verify',
  RESET_PASSWORD: '/auth/reset',
  PROFILE: '/profile',
  TASKS: '/tasks',
  task: (id: string) => `/tasks/${id}`,
  ERROR_404: '/404',
});
