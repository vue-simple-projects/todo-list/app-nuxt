export const appConfig = Object.freeze({
  LOCAL_STORAGE_TOKEN_KEY:
    import.meta.env.VITE_LOCAL_STORAGE_TOKEN_NAME || 'todo-list.access.token',
});
