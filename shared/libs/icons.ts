import userIcon from '~assets/images/user-icon.svg';
import logoutIcon from '~assets/images/logout-icon.svg';
import addListIcon from '~assets/images/add-list-icon.svg';
import removeListIcon from '~assets/images/remove-list-icon.svg';

export const icons = Object.freeze({
  userIcon,
  logoutIcon,
  addListIcon,
  removeListIcon,
});
