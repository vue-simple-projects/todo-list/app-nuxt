export const config = Object.freeze({
  API_URL: import.meta.env.VITE_API_URL || 'http://localhost:5700/api',
  API_BODY_HEADERS: [
    { name: 'Content-Type', value: 'application/json' },
    { name: 'Accept', value: 'application/json' },
  ],
});
