import { apiEndpoints } from './endpoints';
import { appConfig } from '~shared/libs';
import { config } from './config';

type Method = 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH';
type Options = {
  method: string;
  headers: Headers;
  credentials: 'include';
  body?: string;
};

export const api = {
  get: request('GET'),
  post: request('POST'),
  put: request('PUT'),
  delete: request('DELETE'),
  patch: request('PATCH'),
};

function request(method: Method) {
  return async <T extends object>(
    resource: string,
    body: unknown = undefined,
  ): Promise<T> => {
    const options = makeOptions(method, body);
    const url = config.API_URL + resource;
    return await send<T>(url, options);
  };
}

function makeOptions(method: Method, body: unknown): RequestInit {
  const options = {
    method,
    headers: new Headers(),
    credentials: 'include',
  } as Options;
  trySetBody(options, body);
  trySetAuthorization(options);

  return options;
}

function trySetBody(options: Options, body: unknown) {
  if (!!body) {
    config.API_BODY_HEADERS.forEach((e) =>
      options.headers.append(e.name, e.value),
    );
    options.body = JSON.stringify(body);
  }
}

function trySetAuthorization(options: Options) {
  const token = localStorage.getItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
  if (!!token) {
    if (options.headers.has('Authorization')) {
      options.headers.delete('Authorization');
    }
    options.headers.append('Authorization', `Bearer ${token}`);
  }
}

async function send<T>(url: string, options: RequestInit): Promise<T> {
  const response = await fetch(url, options);
  if (!response.ok) {
    if (response.status === 401) {
      await tryRefreshToken();
      trySetAuthorization(options as Options);
      return await send<T>(url, options);
    } else {
      const data = await response.json();
      return Promise.reject(data);
    }
  }

  const data = response.status !== 204 ? await response.json() : undefined;
  return data as T;
}

async function tryRefreshToken() {
  window.localStorage.removeItem(appConfig.LOCAL_STORAGE_TOKEN_KEY);
  const options = {
    method: 'GET',
    headers: {},
    credentials: 'include',
  } as RequestInit;
  const url = config.API_URL + apiEndpoints.AUTH;
  const response = await fetch(url, options);
  if (response.ok) {
    const data = await response.json();
    window.localStorage.setItem(appConfig.LOCAL_STORAGE_TOKEN_KEY, data.token);
  } else {
    const data = await response.json();
    const error = (data && data.message) || response.statusText;
    return Promise.reject(error);
  }
}
