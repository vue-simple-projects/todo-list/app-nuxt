export type IDropdownElem = {
  title: string;
  icon: string;
  action: () => void;
};
