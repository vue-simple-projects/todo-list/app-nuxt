import PopupFactory from './PopupFactory.vue';
import ErrorPopup from './ErrorPopup.vue';

export { PopupFactory, ErrorPopup };
