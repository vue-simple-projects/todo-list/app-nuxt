import { defineAsyncComponent } from 'vue';

export const authForms = [
  {
    id: 'login',
    component: defineAsyncComponent(async () => {
      const { LoginForm } = await import('~/widgets/auth');
      return LoginForm;
    }),
  },
  {
    id: 'registration',
    component: defineAsyncComponent(async () => {
      const { RegistrationForm } = await import('~/widgets/auth');
      return RegistrationForm;
    }),
  },
  {
    id: 'recovery',
    component: defineAsyncComponent(async () => {
      const { RecoveryForm } = await import('~/widgets/auth');
      return RecoveryForm;
    }),
  },
  {
    id: 'reset',
    component: defineAsyncComponent(async () => {
      const { ResetPasswordForm } = await import('~/widgets/auth');
      return ResetPasswordForm;
    }),
  },
  {
    id: 'verify',
    component: defineAsyncComponent(async () => {
      const { VerifyEmailForm } = await import('~/widgets/auth');
      return VerifyEmailForm;
    }),
  },
];
