import LoginForm from './LoginForm.vue';
import RegistrationForm from './RegistrationForm.vue';
import RecoveryForm from './RecoveryForm.vue';
import VerifyEmailForm from './VerifyEmailForm.vue';
import ResetPasswordForm from './ResetPasswordForm.vue';

export {
  LoginForm,
  RegistrationForm,
  RecoveryForm,
  VerifyEmailForm,
  ResetPasswordForm,
};
